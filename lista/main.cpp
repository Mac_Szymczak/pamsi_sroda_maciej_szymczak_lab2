#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;


class uczen   //klasa reprezentujaca dane ucznia
{
public:
string imie;
string nazwisko;
int wiek;
double ocena;
uczen *nastepny;  //wskaznik na nastepny element w liscie
uczen()  //konstruktor ktory jest wywolywany przy tworzeniu elementu na liscie
    {
        cout<<"Podaj imie:"<<endl;  //wczytywanie danych
        cin>>imie;
        cout<<"Podaj nazwisko:"<<endl;
        cin>>nazwisko;
        cout<<"Podaj wiek:"<<endl;
        cin>>wiek;
        cout<<"Podaj ocene:"<<endl;
        cin>>ocena;
        nastepny=NULL;  //wskaznik na 0
    }
    void wypisz()
    {
        cout<<"Imie:" <<imie <<endl;
        cout<<"Nazwisko:" <<nazwisko <<endl;
        cout<<"Wiek:" <<wiek <<endl;
        cout<<"Ocena:" <<ocena <<endl;

    }
private:
};
void dodawanie(uczen **poczatek)
{
    uczen *nowa=new uczen;  //stworzenie nowego ucznia
    uczen *pomoc1=(*poczatek), *pomoc2=NULL;  //wskazniki pomocnicze do poruszania sie po liscie
    while(pomoc1!=NULL && (pomoc1->nazwisko).compare(nowa->nazwisko)==-1) //poruszamy sie dopoki nie wyszlismy zaliste i nazwisko
        //na liscie
    {
        pomoc2=pomoc1; //pomoc 2 zawsze wskazuje na pomoc 1
        pomoc1=pomoc1->nastepny;
    }
    if(pomoc1!=NULL && (pomoc1->nazwisko).compare(nowa->nazwisko)==0) //sprawdzenie, czy podany uczen z takim samym nazwiskiem istnieje
    {
        cout<< "Uczen"<<nowa->nazwisko<< "juz istnieje w katalogu";
        delete nowa; //usuniecie
    }
    else
    {
        nowa->nastepny=(*poczatek); //new element bedzie poczatkiem
        (*poczatek)=nowa;
    }
}
void wyswietl(uczen *poczatek)
{
	if(poczatek!=NULL)
	{
		cout <<"Zawartosc:"<<endl;
		while(poczatek)
		{
			poczatek->wypisz();  //wypisanie
			poczatek=poczatek->nastepny; //przejscie na kolejny element
		}
	}
}
bool usun(uczen **poczatek, string a)
{
    if((*poczatek)==NULL) //jesli lista jest pusta
        return false;
    uczen *pomoc1=(*poczatek), *pomoc2=NULL; //wskazniki pomocnicze
    while(pomoc1!=NULL && (pomoc1->nazwisko).compare(a)!=0)
        //przeszukujemy dopoki nie przeleci calego lub nie znajdzie nazwiska
    {
        pomoc2=pomoc1;
        pomoc1=pomoc1->nastepny;
    }
    if(pomoc1==NULL) //nie ma takiego nazwiska
        return false;
    else if(pomoc1==(*poczatek)) //usuwamy koniec
    {
        (*poczatek)=(*poczatek)->nastepny;
        delete pomoc1;
    }
    return true;
}

void usunwszystkie(uczen **poczatek)
{
    if((*poczatek)==NULL){ //warunek dla pustej listy
            cout << " Lista byla juz pusta";
    }

    uczen *pomoc1=*poczatek; //stworzenie dwoch zmiennych pomocniczych jedna wskazujaca na pczatek druga na NULL
    uczen *pomoc2=NULL;
    while(pomoc1!=NULL)
    {
        pomoc2=pomoc1; //przypisanie pomocy 2 do pomocy 1
        pomoc1=pomoc1->nastepny;
        delete pomoc2; //usuniecie elementu
    }
	*poczatek=NULL;

}

int main()
{
    char opcja;
    string nazwisko;
    uczen *poczatek=NULL;

    cout << "Lista przechowujaca elementy roznego typu(Imie,Nazwisko-string, Wiek-int, Ocena-double)" <<endl;
    cout << "d-Dodaj element do listy" <<endl;
    cout << "u-Usun element z listy" <<endl;
    cout << "w-Wyswietl liste" <<endl;
    cout << "r-Usun wszystkie elementy z listy" <<endl;

    while(cin>>opcja)
    {
        switch(opcja)
        {
        case 'd':
            dodawanie(&poczatek);
            break;
        case 'u':
            cout<<"Podaj nazwisko ucznia:";
            cin>>nazwisko;
            if(!usun(&poczatek, nazwisko))
               cout <<"Nie odnaleziono takiego ucznia" <<endl;
               else
                cout <<"Uczen zostal usuniety" <<endl;
            break;
        case 'w':
            wyswietl(poczatek);
            break;
        case 'r':
            usunwszystkie(&poczatek);
            break;

        default:
            cout<< "Wybrales zla opcje, jeszcze raz " <<endl;
        }
    }
    return 0;
}
