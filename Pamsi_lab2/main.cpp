#include <iostream>
#include <vector>
#include <string>
using namespace std;
vector <string> lista;
bool jestPal(string testStr)  //funkcja rekurencyjna sprawdzajaca czy dany wyraz jest palindromem
{
int i=0; //zmienna indeksu pierwszej litery badanego słowa
    int j=testStr.length()-1; //zmienna indeksu ostatniej litery badanego słowa
    //szczególny przypadek
    //jeżeli słowo zawiera dwa takie same znaki lub jest tylko jednym znakiem to słowo jest palindromem
    if(((j-i == 1) || (i==j)) && (testStr[i] == testStr[j])) return true;
    //funkcja rekurencyjna porównująca przeciwległe litery słowa
    else if(testStr[i]==testStr[j])
    {
        //funkcja rekurencyjnie bada wyraz pomniejszony o pierwszą i ostatnią literę niezależnie od długości wyrazu
        //wyraz.substr zwraca sring rozpoczynający się od litery z indeksem 1 oraz o długości pomniejszonej o 2
        //(ponieważ indeksowanie rozpoczyna się od 0)
        return jestPal(testStr.substr(1,(testStr.length()-2)));
    }
    //jeżeli którekolwiek przeciwległe litery nie są takie same to funkcja zwraca fałsz
    else return false;
}
double ilosc=0;  //zerujemy ilosc permutacji
void permutacje(string slowo, int i) //funkcja wykonujaca permutacje
{
    char tymczasowa;  //deklaracja zmiennej
    if(i<(slowo.length()-1))
    {
        for (int j=i; j<slowo.length(); j++)
        {
            tymczasowa=slowo[i];
            slowo[i]=slowo[j];
            slowo[j]=tymczasowa;
            permutacje(slowo, i+1);
        }
        tymczasowa=slowo[i];
        for(int j=i; j<(slowo.length()-1); j++)
            slowo[j]=slowo[j+1];
        slowo[slowo.length()-1]=tymczasowa;

    }
    else
    {
        ilosc++;  //zwiekszamy ilosc permutacji
        if(jestPal(slowo))
            lista.push_back(slowo);
    }
}
void usuwanie() //funkcja usuwajaca duplukaty po zrobionych permutacjach
{
    for(int i=0; i<lista.size(); i++)
    {
        for(int j=i+1; j<lista.size(); j++)
        {
            if(lista[i]==lista[j])
            {
                lista.erase(lista.begin()+j);
            }
        }
    }
}
int main()
{
    string slowo;
    cout << "Podaj slowo do sprawdzemia permutacji:"<<endl;
    cin >>slowo;
    permutacje(slowo,0);
    cout <<ilosc << ":tyle z podanego slowa wykonano permutacji" <<endl;
    cout <<"Ilosc elementow bedacych palindromem:" <<lista.size()<<endl;
    for(vector<string>::iterator it=lista.begin(); it!=lista.end(); it++)
        cout << *it <<endl;
    usuwanie();
    cout <<"Elementy bedace palindromem po usunieciu powtorzen:" <<lista.size() <<endl;
    for(vector<string>::iterator it=lista.begin(); it!=lista.end(); it++)
        cout << *it <<endl;
    lista.clear();
    return 0;
}

