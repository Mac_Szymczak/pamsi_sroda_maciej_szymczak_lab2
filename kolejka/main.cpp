#include <iostream>


using namespace std;

class uczen  //klasa reprezentujaca dane ucznia
{
    public:
string imie;
string nazwisko;
int wiek;
double ocena;
uczen *nastepny; //wskaznik na nastepny element kolejki
uczen *poprzedni; //na poprzedni element kolejki
private:

};
int main()
{
    uczen *wierzcholek_kolejki=NULL;  //kolejka jest pusta
    uczen *koniec_kolejki=NULL;  //kolejka jest pusta
    uczen *pomoc=NULL; //wskaznik do pomocy w obslugiwaniu kolejki
    cout <<"Kolejka, wybierz jedna z opcji" <<endl;
    cout << "d-Dodaj element do kolejki" <<endl;
    cout << "u-Usun element z kolejki" <<endl;
    cout << "w-Wyswietl kolejke" <<endl;
    cout << "r-Usun wszystkie elementy z kolejki" <<endl;
    char opcja;
    while(cin>>opcja)
    {
        switch(opcja)
        {
        case 'd':
            pomoc= new uczen;  //tworzenie nowego obiektu z danymi
            cout<<"wprowadz imie"<<endl;
            cin>> pomoc->imie;
            cout<<"wprowadz nazwisko"<<endl;
            cin>> pomoc->nazwisko;
            cout<<"wprowadz wiek"<<endl;
            cin>> pomoc->wiek;
            cout<<"wprowadz ocene"<<endl;
            cin>> pomoc->ocena;
            cout <<"wprowadzono dane" <<endl;
            if(wierzcholek_kolejki==NULL) //warunek sprawdzajacy jesli kolejka jest pusta
            {
                pomoc->nastepny=pomoc->poprzedni=NULL;
                koniec_kolejki=wierzcholek_kolejki=pomoc;  //jesli warunek jest prawdziwy poczatek i koniec to te same elementy
            }
            else  //jesli kolejka ma juz elementy
            {
              pomoc->nastepny=NULL;
              koniec_kolejki->nastepny=pomoc;    //dodajemy element na koniec kolejki i ustawiamy wskaznik na niego w elemencie ktory byl ostatni
              pomoc->poprzedni=koniec_kolejki;
              koniec_kolejki=pomoc;  //usuwamy koniec kolejki
            }
            break;
        case 'u':
            if(wierzcholek_kolejki!=NULL)
            {
                pomoc=wierzcholek_kolejki;  //ustawiamy zmienna pomocnicza na wierzcholek kolejki
                if(wierzcholek_kolejki==koniec_kolejki) //warunek ze jesli kolejka ma jeden element
                    wierzcholek_kolejki=koniec_kolejki=NULL; //to kolejka teraz bedzie pusta
                else
                    wierzcholek_kolejki=wierzcholek_kolejki->nastepny; //przestawienie wierzcholka na kolejny element
                delete pomoc; //usuniecie elementu ze szczytu
            }
            else
                cout <<"Kolejka jest pusta" <<endl;
            break;
        case 'w':
            if(wierzcholek_kolejki!=NULL)  //jesli kolejka nie jest pusta
            {
                cout<<"Zawartosc kolejki:" <<endl;
                pomoc=wierzcholek_kolejki;
                while(pomoc!=NULL)
                {
                    cout<< "Imie:"<<pomoc->imie <<endl;
                    cout<<"Nazwisko:"<<pomoc->nazwisko <<endl;
                    cout<<"Wiek:"<<pomoc->wiek <<endl;
                    cout<<"Ocena:"<<pomoc->ocena <<endl;
                    pomoc=pomoc->nastepny;  //przechodzimy na kolejny element
                }
            }
            else
                cout<<"Kolejka jest pusta" <<endl;
            break;
        case 'r':
    {
        uczen *pomoc1 = wierzcholek_kolejki;  //tworzymy dwie zmienne pomocnicze
        uczen *pomoc2;

        do
        {
            pomoc2 = pomoc1 -> nastepny;  //petla dziala i usuwa do tego czasu, az pomoc1=pomoc2
            delete(pomoc1);
            pomoc1 = pomoc2;
        }while(pomoc1);

        wierzcholek_kolejki = NULL;
    }

            break;
        default:
            cout<<"Wybierz jedna z opcji, jeszcze raz" <<endl;
            break;
        }
    }
    return 0;
}
